*The [original project][gitlab] is hosted on [Gitlab](https://gitlab.com). You may see it in action on the [demo / info page][gl-page]*

# post Gitlab issues
is a javascript utility that will enable a posting issues (to Gitlab) tool on any web page.

## install
1. grab pgi.js and pgi_config.js in your project
2. add jquery, font awesome, pgi.js and pgi_config.js to your page `<script type=....>`
3. edit pgi_config.js (see http://sycom.gitlab.io/post-Gitlab-issues/#config for more details and double check **warning** there)
4. add `pgiButton` class to any element of your web page you want to be an activator

## Dependencies / Sources / licences
* **Code** : you will need
   * [jQuery](https://jquery.org) - Licence MIT
   * [Font Awesome](http://fontawesome.io) - Licences SIL OFL 1.1 and MIT

All of the codes are **reusable under [MIT](http://opensource.org/licenses/MIT) license**.

## Parcours
* **[gitlab][gitlab]**
    * *online* > [gitlab.io page][gl-page] ![build on gitlab](http://gitlab.com/sycom/post-Gitlab-issues/badges/online/build.svg)
    * [framagit][framagit]

[gitlab]:https://gitlab.com/sycom/post-Gitlab-issues.git
[gl-page]:http://sycom.gitlab.io/post-Gitlab-issues
[framagit]:https://framagit.org/sycom/post-Gitlab-issues.git
